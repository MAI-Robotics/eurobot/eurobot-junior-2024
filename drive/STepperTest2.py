import RPi.GPIO as GPIO
from time import sleep

# Motor 1
DIR = 8
STEP = 10

# Motor 2
DIR2 = 12
STEP2 = 14

# Motor 3
DIR3 = 16
STEP3 = 18

CW = 1
CCW = 0

durationfwd = 5000

delay = .0005

cylces = 1
cyclecount = 0

GPIO.setmode(GPIO.BCM)

GPIO.setup(DIR, GPIO.OUT)
GPIO.setup(STEP, GPIO.OUT)
GPIO.setup(DIR2, GPIO.OUT)
GPIO.setup(STEP2, GPIO.OUT)
GPIO.setup(DIR3, GPIO.OUT)
GPIO.setup(STEP3, GPIO.OUT)

GPIO.output(DIR, CW)


def forward():
    GPIO.output(DIR2, CW)
    GPIO.output(DIR3, CW)

    for x in range(durationfwd):
        GPIO.output(STEP2, GPIO.HIGH)
        GPIO.output(STEP3, GPIO.HIGH)
        sleep(delay)
        GPIO.output(STEP2, GPIO.LOW)
        GPIO.output(STEP3, GPIO.LOW)
        sleep(delay)
        return


def backwards():
    GPIO.output(DIR2, CCW)
    GPIO.output(DIR3, CCW)

    for x in range(durationfwd):
        GPIO.output(STEP2, GPIO.HIGH)
        GPIO.output(STEP3, GPIO.HIGH)
        sleep(delay)
        GPIO.output(STEP2, GPIO.LOW)
        GPIO.output(STEP3, GPIO.LOW)
        sleep(delay)
        return


def southeast():
    GPIO.output(DIR, CW)
    GPIO.output(DIR3, CW)

    for x in range(durationfwd):
        GPIO.output(STEP, GPIO.HIGH)
        GPIO.output(STEP3, GPIO.HIGH)
        sleep(delay)
        GPIO.output(STEP, GPIO.LOW)
        GPIO.output(STEP3, GPIO.LOW)
        sleep(delay)
    return


def southwest():
    GPIO.output(DIR, CCW)
    GPIO.output(DIR3, CCW)

    for x in range(durationfwd):
        GPIO.output(STEP, GPIO.HIGH)
        GPIO.output(STEP2, GPIO.HIGH)
        sleep(delay)
        GPIO.output(STEP, GPIO.LOW)
        GPIO.output(STEP2, GPIO.LOW)
        sleep(delay)
    return


def northeast():
    GPIO.output(DIR, CW)
    GPIO.output(DIR2, CW)

    for x in range(durationfwd):
        GPIO.output(STEP, GPIO.HIGH)
        GPIO.output(STEP2, GPIO.HIGH)
        sleep(delay)
        GPIO.output(STEP, GPIO.LOW)
        GPIO.output(STEP2, GPIO.LOW)
        sleep(delay)
        return


def northwest():
    GPIO.output(DIR, CCW)
    GPIO.output(DIR3, CCW)

    for x in range(durationfwd):
        GPIO.output(STEP, GPIO.HIGH)
        GPIO.output(STEP3, GPIO.HIGH)
        sleep(delay)
        GPIO.output(STEP, GPIO.LOW)
        GPIO.output(STEP3, GPIO.LOW)
        sleep(delay)
        return


def clockwise():
    GPIO.output(DIR, CW)
    GPIO.output(DIR2, CW)
    GPIO.output(DIR3, CW)

    for x in range(durationfwd):
        GPIO.output(STEP, GPIO.HIGH)
        GPIO.output(STEP2, GPIO.HIGH)
        GPIO.output(STEP3, GPIO.HIGH)
        sleep(delay)
        GPIO.output(STEP, GPIO.LOW)
        GPIO.output(STEP2, GPIO.LOW)
        GPIO.output(STEP3, GPIO.LOW)
        sleep(delay)
        return


def counterclockwise():
    GPIO.output(DIR, CCW)
    GPIO.output(DIR2, CCW)
    GPIO.output(DIR3, CCW)

    for x in range(durationfwd):
        GPIO.output(STEP, GPIO.HIGH)
        GPIO.output(STEP2, GPIO.HIGH)
        GPIO.output(STEP3, GPIO.HIGH)
        sleep(delay)
        GPIO.output(STEP, GPIO.LOW)
        GPIO.output(STEP2, GPIO.LOW)
        GPIO.output(STEP3, GPIO.LOW)
        sleep(delay)
        return


while cyclecount < cylces:
    forward()
    backwards()
    southwest()
    northwest()
    southeast()
    northeast()
    clockwise()
    counterclockwise()
    cyclecount = (cyclecount + 1)

GPIO.cleanup()
print('Cycling complete')
